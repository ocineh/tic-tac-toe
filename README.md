# Tic Tac Toe

A version of tic-tac-toe made in rust that is played in a terminal.

## Roadmap

- [X] Display the tray
- [X] Check if there is a winner
- [X] Play a move according to a chosen cell
- [X] Playing a random move
- [X] Request a cell phone number from a player
- [X] Playing a game against another person
- [X] Play a game against the computer with random moves
- [X] Start a random game (computer vs computer)
- [X] Run multiple random games (computer vs computer)
- [ ] Launching threads that launch multiple random games
- [ ] Be able to launch the program with a command line